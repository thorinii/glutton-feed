const express = require('express')
const fs = require('fs')
const path = require('path')
const promisify = require('util').promisify
const bodyParser = require('body-parser')
const RSS = require('rss')
const md5 = require('md5')
const axios = require('axios')
const cheerio = require('cheerio')
const { URL } = require('url')

const FEED_NAME = process.env.FEED_NAME || 'Glutton Feed dev'
const FEED_SITE_URL = process.env.FEED_URL || 'http://localhost:3000/'

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/feed', async (req, res) => {
  try {
    const feedItems = await readDb()

    const feed = new RSS({
      title: FEED_NAME,
      description: 'A greedy agglutinative aggregator of audio content from the web',
      generator: 'Glutton Feed',
      feed_url: new URL('feed', FEED_SITE_URL).toString(),
      site_url: FEED_SITE_URL,
      managingEditor: 'Curated by Lachlan Phillips',
      webMaster: 'Lachlan Phillips',
      language: 'en',
      ttl: 1 * 60 // minutes
    })

    feedItems.forEach(item => {
      const title = item.title || item.url
      feed.item({
        title: title,
        description: 'Link to ' + (item.title || 'untitled') + ' (' + item.url + ')',
        url: item.url,
        guid: md5(item.url + item.timestamp),
        date: item.timestamp,
        enclosure: {
          url: item.url
        }
      })
    })

    res.type('xml').send(feed.xml({ indent: true }))
  } catch (e) {
    console.error('Failed to render feed', e)
    res.status(500).send('Failed to render feed')
  }
})

app.post('/auth/add', async (req, res) => {
  try {
    await addItem(req.query.url, req.query.title || null)

    res.status(201).send('Added item')
  } catch (e) {
    console.error('Failed to add item', e)
    res.status(500).send('Failed to add item')
  }
})

app.post('/auth/add-scrape', async (req, res) => {
  try {
    const items = await scrapePage(req.query.url)

    await addItems(items)

    res.status(201).send('Added ' + items.length + ' items')
  } catch (e) {
    console.error('Failed to add items', e)
    res.status(500).send('Failed to add items')
  }
})

app.use(function (err, req, res, next) {
  console.error(err)
  res.status(500).json({ error: err })
})

app.listen(3000, function () {
  console.log('Started Glutton Feed on port 3000')
})

async function addItem (url, title = null) {
  await addItems([{ title, url }])
}

async function addItems (items) {
  if (items.length === 0) return

  const timestamp = new Date().toISOString()

  items.forEach((i, idx) => {
    if (!i.url || !canParseUrl(i.url)) {
      throw new Error('Invalid item URL: ' + i.url)
    }

    if (!i.timestamp) {
      i.timestamp = new Date(Date.parse(timestamp) - (items.length - idx) * 1000).toISOString()
    }
  })

  const feedItems = await readDb()
  items.forEach(i => {
    feedItems.push({ timestamp: i.timestamp, title: i.title, url: i.url })
  })
  await writeDb(feedItems)
}

async function scrapePage (url) {
  const response = await axios.get(url)
  const page = response.data
  const $ = cheerio.load(page)

  return $('a')
    .filter((idx, tag) => {
      const href = $(tag).attr('href')
      return href && href.endsWith('.mp3')
    })
    .map((idx, tag) => {
      tag = $(tag)
      return {
        url: tag.attr('href'),
        title: (tag.text() || '').trim()
      }
    })
    .toArray()
}

async function readDb () {
  const dbFile = path.join(__dirname, 'data', 'db.json')
  const text = await catchEnoent(() => promisify(fs.readFile)(dbFile, 'utf8'), '')
  if (text === '') return []
  return JSON.parse(text)
}

async function writeDb (db) {
  const dbFile = path.join(__dirname, 'data', 'db.json')
  const tmpFile = path.join(__dirname, 'data', 'db.json.tmp')
  const backupFile = path.join(__dirname, 'data', 'db.json.bak')
  const json = JSON.stringify(db, null, '  ') + '\n'

  await promisify(fs.writeFile)(tmpFile, json)

  await catchEnoent(() => promisify(fs.rename)(dbFile, backupFile))
  await promisify(fs.rename)(tmpFile, dbFile)
  await catchEnoent(() => promisify(fs.unlink)(backupFile))
}

async function catchEnoent (fn, def = null) {
  try {
    return await fn()
  } catch (e) {
    if (e.code === 'ENOENT') return def
    else throw e
  }
}

function canParseUrl (urlString) {
  try {
    return !!new URL(urlString)
  } catch (e) {
    return false
  }
}
