#!/usr/bin/env bash
set -e

CONFIG_FILE="$1"
if [[ ! -f "${CONFIG_FILE}" ]]; then
  echo "Config file must exist: ${CONFIG_FILE}"
  exit 1
fi


. "${CONFIG_FILE}"


sudo podman rm -f "${CONTAINER}" || true

sudo podman run -d \
  --name "${CONTAINER}" \
  --restart=always \
  --network "${NETWORK}" \
  -e "FEED_SITE_URL=${APP_URL}" \
  -e "FEED_NAME=${FEED_NAME}" \
  -v "${STORAGE}:/app/data:rw,z" \
  lachlanap/gluttonfeed:latest
