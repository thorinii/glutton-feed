#!/usr/bin/env bash
set -e
BASE_DIR="$(cd "$(dirname "$0")"; pwd)"

sudo buildah bud \
  --tag lachlanap/gluttonfeed \
  "${BASE_DIR}"
